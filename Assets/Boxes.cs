﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boxes : MonoBehaviour {

    public static int gridWeight = 10;
    public static int gridHeight = 20;
    public static Transform[,] grid = new Transform[gridWeight, gridHeight];//игровое поле
    public GameObject new_box;

    public static Vector2 round(Vector2 v)//округление до целых координат
    {
        return new Vector2(Mathf.Round(v.x), Mathf.Round(v.y));
    }

    public static bool isInsideGrid(Vector2 pos) {//проверяем правельность позиции (в игровых рамках или нет)
		return (((int)pos.x >= 0) && ((int)pos.x < gridWeight) && ((int)pos.y >= 0));
	}

    public static void Delete(int y) {//удаляем строку
		for (int x = 0; x < gridWeight; ++x) {
			Destroy(grid[x, y].gameObject);
			grid[x, y] = null;
		}
	}

    public static bool isFull(int y) {//проверяем заполнилась ли строка
		for (int x = 0; x < gridWeight; ++x)
			if (grid[x, y] == null)
				return false;
		return true;
	}

    public static void DeleteRow() {//удаляем полные строки
		for (int y = 0; y < gridHeight; ++y) {
			if (isFull(y)) {
				Delete(y);
				RowDownAll(y+1);
				--y;
			}
		}
	}

    public static void RowDown(int y) {//опускаем строку
		for (int x = 0; x < gridWeight; ++x) {
			if (grid[x, y] != null) {
				grid[x, y-1] = grid[x, y];
				grid[x, y] = null;
				grid[x, y-1].position += new Vector3(0, -1, 0);
			}
		}
	}

    public static void RowDownAll(int y) {//опускаем все строки
		for (int i = y; i < gridHeight; ++i)
			RowDown(i);
	}

    float fall = 0;
    bool f;
	void Start () {
        f = true;
	}

	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown(KeyCode.RightArrow)) //нажали стрелку вправо
        {
            transform.position += new Vector3(1, 0, 0);
            if (isValidPosition())//проверяем корректна ли позиция
            {
                GridUpdate();
            }
            else
            {
                transform.position += new Vector3(-1, 0, 0);//если не коректна, возвращаем обратно
            }
        }

        else
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow)) //влево
            {
                transform.position += new Vector3(-1, 0, 0);
                if (isValidPosition())
                {
                    GridUpdate();
                }
                else
                {
                    transform.position += new Vector3(1, 0, 0);
                }
            }

            else
            {
                if (Input.GetKeyDown(KeyCode.UpArrow)) //вверх
                {
                    transform.Rotate(0, 0, -90);
                    if (isValidPosition())
                    {
                        GridUpdate();
                    }
                    else
                    {
                        transform.Rotate(0, 0, 90);
                    }
                }

                else
                {
                    if ((Input.GetKeyDown(KeyCode.DownArrow)) || (Time.time - fall >= 0.5)) //вниз или с каждым тиком
                    {
                        transform.position += new Vector3(0, -1, 0);
                        if (isValidPosition())
                        {
                            GridUpdate();
                            f = false;
                        }
                        else
                        {
                            if (f) { Application.LoadLevel(0); }
                            transform.position += new Vector3(0, 1, 0);
                            DeleteRow();
                            //Debug.LogWarning("aa");
                            Instantiate(new_box, new Vector3(5, 19, 0), Quaternion.identity); ;
                            enabled = false;
                        }
                        fall = Time.time;
                    }
                }
            }
        }
	}

    	bool isValidPosition() 
        {        
		    foreach (Transform child in transform) 
            {
                
			    Vector2 v = round(child.position);//получаем позицию кубика
			    if (!isInsideGrid(v))//проверяем вхождение в игровое поле
				    return false;
			    if ((grid[(int)v.x, (int)v.y] != null) &&
                    (grid[(int)v.x, (int)v.y].parent != transform))//если клетка занята
				    return false;
		    }
		    return true;
	    }

        void GridUpdate()//обновление игровой таблицы
        {
            for (int y = 0; y < gridHeight; ++y)
                for (int x = 0; x < gridWeight; ++x)
                    if (grid[x, y] != null)
                        if (grid[x, y].parent == transform)
                            grid[x, y] = null;
            foreach (Transform child in transform)
            {
                Vector2 v = round(child.position);
                grid[(int)v.x, (int)v.y] = child;
            }
        }

}
