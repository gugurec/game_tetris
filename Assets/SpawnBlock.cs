﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBlock : MonoBehaviour {

    public GameObject[] boxList;

    void Start()
    {
        SpawnNewBox();
        Destroy(gameObject);
    }

    public void SpawnNewBox()
    {
        int i = Random.Range(0, boxList.Length);
        Instantiate(boxList[i], transform.position, Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
